package Calculator;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Calculations 
{
	private static String curNum= "";
	private static boolean point = false;
	private static ArrayList <String> equations = new ArrayList <String>();
	private static boolean carryOver = false;
	private static boolean sign = false;
	private static boolean equal = false;
	private static boolean num = true;
	final private static char BACKSPACE ='\u2190';
	private static Double num1 = 0.0;
	private static Double num2 = 0.0;
	private static String task = "";
	private static String result = "";
	
	
	
	public Calculations () 
	{
		
	}
		
	public static String inputChar (String input) 
	{
		String retString ="";
		
		int value = input.charAt(0);
		
		if (value >= '0' && value <= '9') 
		{
			if (carryOver) 
				{													
					curNum = "";
					carryOver = false;
					sign = true;	
					equal = true;
					curNum += input;
					return curNum;	
				}

			else 
				{	
					sign = true;
					equal = true;
					curNum += input;
					return curNum;					
				}
		}
		if (input == "MR")
		{
			value = 'A';
		}
		if(input == "M+")
		{
			value = 'B';
		}
		if(input == "M-")
		{
			value = 'D';
		}
		if(input == "MC")
		{
			value = 'E';
		}
		
		
		switch (value) 
		{	
			case '.' :
			if (carryOver) 
				{													
					curNum = "";
					carryOver = false;	
				}	
			else if (! point) 
				{													
					curNum += input;
					point = true;
					retString = curNum;			
				}
			retString = curNum;
			break;
				
			case '-':
				if (sign && curNum.length() > 0) 
				{
					carryOver = false;
					saveNum1(curNum);
					saveNum1(input);
					curNum = "";													
					carryOver = false;
					num = true;
					point= false;													
					sign = false;
					equal = false;
				}
				
			case '/':
				if (sign && curNum.length() > 0) 
				{
					carryOver = false;
					saveNum1(curNum);
					saveNum1(input);
					curNum = "";													
					carryOver = false;
					num = true;
					point= false;													
					sign = false;
					equal = false;
				}
				
			case '*':
				if (sign && curNum.length() > 0) 
					{
						carryOver = false;
						saveNum1(curNum);
						saveNum1(input);
						curNum = "";													
						carryOver = false;
						num = true;
						point= false;														
						sign = false;
						equal = false;
					}
			break;
			
			case '+':
				if (sign && curNum.length() > 0) 
				{
					carryOver = false;
					saveNum1(curNum);
					saveNum1(input);
					curNum = "";													
					carryOver = false;
					num = true;
					point= false;													
					sign = false;
					equal = false;
				}
			break;
			
			case '=':																																					
			if (curNum != "" && equal) 
				{  
					saveNum1(curNum);											
					equal = false;	
					curNum = calculate();											
					point= false;													
					carryOver = true;													
					equations.clear();													
					equal = false;
					sign = true;
					num = true;
					retString = curNum;
				} 
			break;
			
			case 'A':	//MR
					switch(task)
					{
						case "M+":
							return ""+num1;
							
						case "M-":
							return ""+num1;
						case "MC":
							return "";
					}
					
					
			case 'B':	//M+
				num1 += Double.parseDouble(curNum);
				carryOver = false;
				curNum = "";
				task = "M+";
			break;
			
			case 'D':	//M-
				num1 = num1 - Double.parseDouble(curNum);
				carryOver = false;
				curNum = "";
				task = "M-";
			break;
			
			case 'E':	//MC
				num1 = 0.0;
				curNum = "";
				task = "MC";
				
				
			case 'C':
				if (input == "CE") 
					{												
						curNum = "";													
						point = false;													
						return "";															
					} 
				else  
					{
						curNum = "";													
						equations.clear();
						point = false;
					} 
			
			case BACKSPACE:															
			if (curNum.length() > 0) 
			{																	
				if (curNum.charAt(curNum.length() -1) == '.') 
					{
						point = false;
					}
				curNum = curNum.substring(0, curNum.length() -1);					
			}
			retString = curNum;
			break;
			
			default:
			return curNum;
			}
		return retString;
		}
	
	private static void saveNumLoop( int index ,String term) 
	{		
		term = "[" + term + "]";
		equations.add(index,term);
	}	
	private static void saveNum1(String term) 
	{			
		term = "[" + term + "]";
		equations.add(term);
	}		
	private static String print (int index) 
	{
		String term = equations.get(index);
		return term.substring(1, term.length() -1);
	}	
	
	public static double divide( double numerator, double denominator )
		      throws DividebyZeroException
		   {
		      if ( denominator == 0 )
		         throw new DividebyZeroException();

		      return ( double ) numerator / denominator;
		   } // End of method divide
	
	
		private static String calculate() 
		{
			double firstNum, secondNum, memory;
			
			while  (equations.contains("[/]")) {
				for (int i = 1; i < equations.size() -1; i++) 
				{
					if (print(i).equals("/")) 
					{
					      try {
					    	  	firstNum = Double.parseDouble(print(i-1));
								secondNum = Double.parseDouble(print(i +1));
								equations.remove(i);
								equations.remove(i);
								equations.remove(i -1);
								saveNumLoop(i-1, String.valueOf(divide(firstNum , secondNum)));
					      }
					      
					      catch ( DividebyZeroException dbze ) 
					      {
					          JOptionPane.showMessageDialog( null, dbze,
					             "Error Message", JOptionPane.ERROR_MESSAGE );
					       }
					    // System.exit(0);
					} 
				} 
			}
			while  (equations.contains("[*]")) 
			{
				for (int i = 1; i < equations.size() -1; i++) 
				{
					if (print(i).equals("*")) 
					{
						firstNum = Double.parseDouble(print(i-1));
						secondNum = Double.parseDouble(print(i +1));
						equations.remove(i);
						equations.remove(i);
						equations.remove(i -1);
						saveNumLoop(i-1, String.valueOf(firstNum * secondNum));
					} 
				}
			} 
			while  (equations.contains("[+]")) 
			{
				for (int i = 1; i < equations.size() -1; i++) 
				{
					if (print(i).equals("+")) 
					{
						firstNum = Double.parseDouble(print(i-1));
						secondNum = Double.parseDouble(print(i +1));
						equations.remove(i);
						equations.remove(i);
						equations.remove(i -1);
						saveNumLoop(i-1, String.valueOf((firstNum + secondNum)));
					} 
				}
			}
			while  (equations.contains("[-]")) 
			{
				for (int i = 1; i < equations.size() -1; i++) 
				{
					if (print(i).equals("-")) 
					{
						firstNum = Double.parseDouble(print(i-1));
						secondNum = Double.parseDouble(print(i +1));
						equations.remove(i);
						equations.remove(i);
						equations.remove(i -1);
						saveNumLoop(i-1, String.valueOf((firstNum - secondNum)));
					} 
				}
			}
			
			return print(0);

		}
}
