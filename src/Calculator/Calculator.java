package Calculator;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;




public class Calculator extends JFrame implements ActionListener
{
	//buttons
	private JButton button;
	private JButton buttonSet1;
	private JButton buttonSet2;
	private JButton btn;
	private String[] btnLabels = {"MC","M+","M-","MR","CE", "C", "\u2190", "/", "7", "8" , "9", "*", "4", "5", "6", "-", "1", "2", "3", "+", "\u00B1", "0" , ".", "="};
	//\u221A - square root unicode
	
	//TextField
	private JTextField display = new JTextField();
	//Panel
	private JPanel buttonPanel = new JPanel();
	private JPanel displayPanel = new JPanel();
	
	public Calculator()
	{
		//Frame
		super("Calculator");
		
		setSize(500, 650);
		setLocation(1000, 300);
		setResizable(false);
		setLayout(new BorderLayout());
		
		//TextField
		display.setPreferredSize(new Dimension(400, 180));
		display.setFont(new Font("Arial", Font.PLAIN, 50));
		display.setEditable(false);
		add("North" , display);
		
		//Buttons
		buttonPanel.setLayout(new GridLayout(6, 4, 1, 1));
		add(buttonPanel , "Center");
		
		
		for(int i = 0; i < btnLabels.length; i++)
			{
				button = new JButton(btnLabels[i]);
				button.setFont(new Font("Arial", Font.PLAIN, 22));
				button.setBorderPainted(false);
				button.setContentAreaFilled(false);
				button.setOpaque(true);
				button.addActionListener(this);
				button.setBackground(new Color(241, 241, 241));
				
				//Sets colour of button operations e.g. addition , subtraction
				if(btnLabels[i] == "CE" || btnLabels[i] == "C" || btnLabels[i] == "\u2190" || btnLabels[i] == "\u00B1" || btnLabels[i] == ".")
					{
						button.addMouseListener(ml);
					}
				//sets colour of memory function buttons
				else if(btnLabels[i] == "MC" || btnLabels[i] == "M+" || btnLabels[i] == "M-" || btnLabels[i] == "MR")
					{
							button.addMouseListener(ml4);
					}
				//Sets colour of commands such as backspace , decimal point
				else if(btnLabels[i] == "/" || btnLabels[i] == "*" || btnLabels[i] == "-" || btnLabels[i] == "+" || btnLabels[i] == "=")
					{
						button.addMouseListener(ml2);
					}
				//sets colour of buttons 0 to 9
				else if(btnLabels[i] == "0" || btnLabels[i] == "1" || btnLabels[i] == "2" || btnLabels[i] == "2" || btnLabels[i] == "3" || btnLabels[i] == "4" || btnLabels[i] == "5" || btnLabels[i] == "6" || btnLabels[i] == "7" || btnLabels[i] == "8" || btnLabels[i] == "9")
				{
					button.addMouseListener(ml3);
				}
				
				buttonPanel.add(button);
			}
		
		//operation to do when the window is closed
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setVisible(true);
	}
	
	//@Override 
	//action listener
	public void actionPerformed(ActionEvent evt)
	{
		String buttonLabel = evt.getActionCommand();
		display.setText(Calculations.inputChar(buttonLabel));
	} 
	
	
	// mouse listeners for when mouse enters and exits a button it will to change to a given colour 
	MouseListener ml = new MouseAdapter()
	{
	    public void mouseEntered(MouseEvent evt)
	    {            
	        Component c = evt.getComponent();
	        c.setBackground(new Color(255, 128, 128));//pink colour
	    }                                      

	    public void mouseExited(MouseEvent evt)
	    {                                      
	        Component c = evt.getComponent();
	        c.setBackground(new Color(241, 241, 241));//gray colour
	    }  
		
	};
	
	MouseListener ml2 = new MouseAdapter()
	{
	    public void mouseEntered(MouseEvent evt)
	    {            
	        Component c2 = evt.getComponent();
	        c2.setBackground(new Color(204, 0, 0)); //red colour
	    }                                      

	    public void mouseExited(MouseEvent evt)
	    {                                      
	        Component c = evt.getComponent();
	        c.setBackground(new Color(241, 241, 241)); //gray colour
	    }  
		
	};
	MouseListener ml3 = new MouseAdapter()
	{
	    public void mouseEntered(MouseEvent evt)
	    {            
	        Component c2 = evt.getComponent();
	        c2.setBackground(new Color(174, 214, 241));//Light Blue Colour
	    }                                      

	    public void mouseExited(MouseEvent evt)
	    {                                      
	        Component c = evt.getComponent();
	        c.setBackground(new Color(241, 241, 241)); //gray colour
	    }  
		
	};
	
	MouseListener ml4 = new MouseAdapter()
	{
	    public void mouseEntered(MouseEvent evt)
	    {            
	        Component c2 = evt.getComponent();
	        c2.setBackground(new Color(230, 92, 0)); //orange colour
	    }                                      

	    public void mouseExited(MouseEvent evt)
	    {                                      
	        Component c = evt.getComponent();
	        c.setBackground(new Color(241, 241, 241)); //gray colour 
	    }  
		
	};
	public static void main(String[]args)
	{
		new Calculator();
	}
}


