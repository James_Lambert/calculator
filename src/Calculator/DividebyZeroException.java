package Calculator;

public class DividebyZeroException extends ArithmeticException 
{
   public DividebyZeroException()
   {
      super( "Attempted to Divide by Zero" );
   }

   public DividebyZeroException( String message )
   {
      super( message );
   }
}
